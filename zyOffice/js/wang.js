window.wangEditor.importpdf = {
    init: function(editor,w) {
        var tool = $(editor).find(" .w-e-toolbar");
        tool.append("<div class='w-e-menu importpdf' title='导入PDF文档'></div>");

        tool.find(".importpdf").click(function(){
            zyOffice.SetEditor(w).api.openPdf();
        });
        return editor;
    }
};
window.wangEditor.importword = {
    init: function(editor,w) {
        var tool = $(editor).find(" .w-e-toolbar");
        tool.append("<div class='w-e-menu importword' title='导入Word文档（docx）'></div>");

        tool.find(".importword").click(function(){
            zyOffice.SetEditor(w).api.openDoc();
        });
        return editor;
    }
};
window.wangEditor.exportword = {
    init: function(editor,w) {
        var tool = $(editor).find(" .w-e-toolbar");
        tool.append("<div class='w-e-menu exportword' title='导出Word文档（docx）'></div>");

        tool.find(".exportword").click(function(){
            zyOffice.SetEditor(w).api.exportWord();
        });
        return editor;
    }
};