window.wangEditor.wordpaster = {
    init: function(editor,w) {
        var tool = $(editor).find(" .w-e-toolbar");
        tool.append("<div class='w-e-menu wordpaster' title='Word一键粘贴'></div>\
        <div class='w-e-menu importwordtoimg' title='Word转图片'></div>\
        <div class='w-e-menu netpaster' title='网络图片一键导入'></div>\
        <div class='w-e-menu wordimport' title='导入Word文档'></div>\
        <div class='w-e-menu excelimport' title='导入Excel文档'></div>\
        <div class='w-e-menu pptimport' title='导入PPT文档'></div>\
        <div class='w-e-menu pdfimport' title='导入PDF文档'></div>");

        tool.find(".wordpaster").click(function(){
            WordPaster.getInstance().SetEditor(w).Paste();
        });
        tool.find(".importwordtoimg").click(function () {
            WordPaster.getInstance().SetEditor(w).importWordToImg();
        });
        tool.find(".netpaster").click(function(){
            WordPaster.getInstance().SetEditor(w).UploadNetImg();
        });
        tool.find(".wordimport").click(function(){
            WordPaster.getInstance().SetEditor(w).importWord();
        });
        tool.find(".excelimport").click(function(){
            WordPaster.getInstance().SetEditor(w).importExcel();
        });
        tool.find(".pptimport").click(function(){
            WordPaster.getInstance().SetEditor(w).importPPT();
        });
        tool.find(".pdfimport").click(function(){
            WordPaster.getInstance().SetEditor(w).ImportPDF();
        });
    }
};