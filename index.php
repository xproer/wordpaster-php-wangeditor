<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>WordPaster form wangEditor3</title>
	<link type="text/css" rel="Stylesheet" href="demo.css" />
	<script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/jquery-1.9.1.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="wangEditor.min.js"></script>
    <script type="text/javascript" src="WordPaster/layer/layer.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/w.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/wang.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyCapture/z.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyCapture/wang.js" harset="utf-8"></script>
    <script type="text/javascript" src="zyOffice/js/o.js" harset="utf-8"></script>
    <script type="text/javascript" src="zyOffice/js/wang.js" harset="utf-8"></script>
    <script type="text/javascript" src="demo.js" charset="utf-8"></script>
    <style type="text/css">
		.wordpaster{background: transparent url(WordPaster/css/paster.png) center center no-repeat;
			width: 32px !important;
			height: 22px !important;
			}
        .importwordtoimg{background: transparent url(WordPaster/css/word1.png) center center no-repeat;
            width: 32px !important;
			height: 22px !important;			
			}
		.netpaster{background: transparent url(WordPaster/css/net.png) center center no-repeat; 
			width: 32px !important;
			height: 22px !important;			
            }
        .wordimport{background: transparent url(WordPaster/css/word2.png) center center no-repeat;
			width: 32px !important;
			height: 22px !important;
			}
        .excelimport{background: transparent url(WordPaster/css/xls.png) center center no-repeat;
			width: 32px !important;
			height: 22px !important;
			}
        .pptimport{background: transparent url(WordPaster/css/ppt.png) center center no-repeat;
			width: 32px !important;
			height: 22px !important;
			}
        .pdfimport {background: transparent url(WordPaster/css/pdf.png) center center no-repeat;
            width: 32px !important;
			height: 22px !important;
			}
        .zycapture {background: transparent url(zyCapture/z.png) center center no-repeat;
            width: 32px !important;
            height: 22px !important;
            }
        .importword {background: transparent url(zyOffice/css/w.png) center center no-repeat;
            width: 32px !important;
            height: 22px !important;
            }
        .exportword {background: transparent url(zyOffice/css/exword.png) center center no-repeat;
            width: 32px !important;
            height: 22px !important;
            }
        .importpdf {background: transparent url(zyOffice/css/pdf.png) center center no-repeat;
            width: 32px !important;
            height: 22px !important;
            }
	</style>
</head>
<body>
	<div style=" font-size:medium; line-height:130%;">
		<p>演示方法：</p>
		<ul style="list-style-type:decimal;">
			<li>打开Word文档，复制多张图片，然后在编辑器中按 Ctrl+V 粘贴，编辑器将自动上传所有图片。</li>
			<li>复制电脑中任意图片文件，然后点击编辑器中的图片粘贴按钮。</li>
			<li>通过QQ或其它软件截屏，复制图片，然后点击编辑器中的图片粘贴按钮。</li>
		</ul>
		<p>相关问题：</p>
		<ul style="list-style-type:decimal;">
			<li><a target="_blank" href="http://www.ncmem.com/webapp/wordpaster/pack.aspx">安装插件</a></li>
			<li><a target="_blank" href="https://www.kancloud.cn/wangfupeng/wangeditor3/332599">wangEditor3教程</a></li>
		</ul>
	</div>
	<div id="demos"></div>
	<div id="editor">
        <p>泽优全平台内容发布解决方案 for php wangEditor3</p>
        <p>泽优Word一键粘贴控件（WordPaster）</p>
        <p>泽优全平台截屏控件（zyCapture）</p>
        <p>泽优Office文档转换服务（zyOffice）</p>
    </div>
    <p>第二个编辑器</p>
	<div id="editor2">
        <p>泽优全平台内容发布解决方案 for php wangEditor3</p>
        <p>泽优Word一键粘贴控件（WordPaster）</p>
        <p>泽优全平台截屏控件（zyCapture）</p>
        <p>泽优Office文档转换服务（zyOffice）</p>
    </div>	
	<script type="text/javascript">
		var pos = window.location.href.lastIndexOf("/");
        var api = [
            window.location.href.substr(0, pos + 1),
            "php/upload.php"
        ].join("");
		WordPaster.getInstance({
			//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
			PostUrl: api,
			//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
            ImageUrl: "",
            //设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
            FileFieldName: "file",
            //提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
            ImageMatch: '',
			Cookie: 'PHPSESSID=<?php echo session_id() ?>',
			event:{
				dataReady:function(e){
					//e.word,
					//e.imgs:tag1,tag2,tag3
					console.log(e.imgs)
				}
			}
		});//加载控件
		
		//zyCapture
        zyCapture.getInstance({
            config: {
                PostUrl: api,
				FileFieldName: "file",
                Fields: { uname: "test" }
            }
        });

        // zyoffice，
        // 使用前请在服务端部署zyoffice，
        // http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c
        zyOffice.getInstance({
            word: 'http://localhost:13710/zyoffice/word/convert',
            wordExport: 'http://localhost:13710/zyoffice/word/export',
            pdf: 'http://localhost:13710/zyoffice/pdf/upload'
        });

	    var E = window.wangEditor;
        var edt = new E('#editor');
        edt.customConfig.zIndex = 100;
        // 或者 var editor = new E( document.getElementById('editor') )
        edt.create();
        E.wordpaster.init('#editor',edt);
        E.zycapture.init('#editor',edt);
        E.importword.init('#editor',edt);
        E.exportword.init('#editor',edt);
        E.importpdf.init('#editor',edt);

        var edt2 = new E('#editor2');
        edt2.customConfig.zIndex = 100;
        // 或者 var editor = new E( document.getElementById('editor') )
        edt2.create();
        E.wordpaster.init('#editor2',edt2);
        E.zycapture.init('#editor2',edt2);
        E.importword.init('#editor2',edt2);
        E.exportword.init('#editor2',edt2);
        E.importpdf.init('#editor2',edt2);

	</script>
</body>
</html>
